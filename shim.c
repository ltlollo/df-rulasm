// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE.

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <dlfcn.h>

int __printf_chk(int, const char *, ...);
int __sprintf_chk(char *, int, size_t, const char *, ...);
char *__strcpy_chk(char *, const char *, size_t);
char *__stpcpy_chk(char *, const char *, size_t);
void *__memcpy_chk(void *, const void *, size_t, size_t);
void *__memmove_chk(void *, const void *, size_t, size_t);
char *__strcat_chk(char *, const char *, size_t);
void *__memset_chk(void *, int, size_t, size_t);
int pthread_create(void *, const void *, void *(*) (void *), void *);

int
__printf_chk(int flag, const char * format, ...) {
    va_list arg;
    va_start(arg, format);
    return vprintf(format, arg);
}

int
__sprintf_chk(char *str, int flag, size_t strlen, const char *format, ...) {
    va_list arg;
    va_start(arg, format);
    return vsprintf(str, format, arg);
}

char *
__strcpy_chk(char *dest, const char *src, size_t destlen) {
     return strcpy(dest, src);
}

char *
__stpcpy_chk(char *dest, const char *src, size_t destlen) {
    return stpcpy(dest, src);
}

void *
__memcpy_chk(void *dest, const void *src, size_t len, size_t destlen) {
    return memcpy(dest, src, len);
}

void *
__memmove_chk(void *dest, const void *src, size_t len, size_t destlen) {
    return memmove(dest, src, len);
}

char *
__strcat_chk(char *dest, const char *src, size_t destlen) {
    return strcat(dest, src);
}

void *
__memset_chk(void *dest, int c, size_t len, size_t destlen) {
    return memset(dest, c, len);
}

int
pthread_create(void *th, const void *__attr, void *(*fn) (void *), void *arg) {
    int (*create)(void *, const void *, void *(*) (void *), void *);
    int (*attr_init)(void *);
    int (*attr_setstacksize)(void *, size_t);
    void *attr[64];

    create = dlsym(RTLD_NEXT, "pthread_create");
    attr_init = dlsym(RTLD_NEXT, "pthread_attr_init");
    attr_setstacksize = dlsym(RTLD_NEXT, "pthread_attr_setstacksize");

    attr_init(attr);
    attr_setstacksize(attr, 8388608);

    return create(th, attr, fn, arg);
}
