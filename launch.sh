#!/usr/bin/env sh

{
    for f in libs/libstdc++.*; do
        mv $f old_$(basename $f).bk
    done
} 2>/dev/null

./intrpch libs/Dwarf_Fortress
LD_PRELOAD=./shim.so ./df
