// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE.

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <err.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

#include "elf.h"

#define xensurem(cond, ...)\
    do {\
        if ((cond) == 0) {\
            errx(1, __VA_ARGS__);\
        }\
    } while (0)
#define xensure(cond)\
        xensurem(cond, "malformed ELF, FILE : \"%s\", LINE : \"%d\""\
            , __FILE__\
            , __LINE__\
        )
#define xinboundpm(ptr, end, ...) xensurem((void *)ptr < (void *)end, __VA_ARGS__)
#define xinboundp(ptr, end) xensure((void *)ptr < (void *)end)
#define xinboundm(ptr, ...) xinboundpm(ptr, program_end, __VA_ARGS__)
#define xinbound(ptr) xinboundp(ptr, program_end)
#define msg(...) (void) fprintf(stderr, __VA_ARGS__)

#define INTERP "/lib/ld-musl-x86_64.so.1"

extern const char *__progname;

static const char *usage = "USAGE: %s ifile intep"
    "\nPARAMS:"
    "\n\tifile<string>: The input elf file"
    "\n\tinterp<string>: The elf interpreter (default: %s)"
    "\n"
    ;

static void
chinterp(struct elf64_ehdr *eh
    , char *restrict program_beg
    , char *restrict program_end
    , char *restrict interp
    ) {
    char *pheader_beg = program_beg + eh->e_phoff;
    char *pheader_end = pheader_beg + eh->e_phnum * eh->e_phentsize;
    char *pheader = pheader_beg;
    ssize_t ilen = strlen(interp);
    ssize_t olen;
    struct elf64_phdr *ep;
    char *segment_beg;
    char *segment_end;
    char *str;

    xinboundm(pheader_beg, "program header too long");
    xensurem(pheader_end <= program_end, "program header offset out of range");

    while (pheader != pheader_end) {
        ep = (void *)pheader;
        segment_beg = program_beg + ep->p_offset;
        segment_end = segment_beg + ep->p_filesz;

        if (segment_beg > program_end) {
            errx(1, "elf file too small");
        }
        if (segment_end > program_end) {
            errx(1, "program segment too big");
        }
        if (__builtin_popcountl(ep->p_align) > 1){
            errx(1, "invalid segment alignment");
        }
        if (ep->p_type == PT_INTERP) {
            str = segment_beg;
            olen = strnlen(str, program_end - str);
            xensure(olen != program_end - str);
            xensure(ilen <= olen);
            if (strcmp(interp, str) != 0) {
                msg("INFO: patching '%s' with '%s'\n", str, interp);
                sprintf(str, "%s", interp);
            } else {
                msg("INFO: nothing to do\n");
            }
            return;
        }
        pheader += eh->e_phentsize;
    }
    errx(1, "PT_INTERP section not found");
}

int
main(int argc, char *argv[]) {
    char *interp = INTERP;
    char *fn;
    int fd;
    off_t len;
    struct elf64_ehdr *eh;
    char *program_beg;
    char *program_end;

    if (argc - 1 < 1) {
        errx(1, usage, __progname, INTERP);
    }
    if (argc - 1 > 1) {
        interp = argv[2];
    }
    fn = argv[1];
    fd = open(fn, O_RDWR);

    if (fd == -1) {
        err(1, "open");
    }
    len = lseek(fd, 0, SEEK_END);

    if (len == -1) {
        err(1, "lseek");
    }
    if (lseek(fd, 0, SEEK_SET) == -1) {
        err(1, "lseek");
    }
    program_beg = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    program_end = program_beg + len;
    eh = (void *)program_beg;

    if (program_beg == MAP_FAILED) {
        err(1, "program_begap");
    }
    xensure(eh->e_phoff != 0);
    chinterp(eh, program_beg, program_end, interp);
    return 0;
}

