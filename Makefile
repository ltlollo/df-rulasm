all: intrpch shim

shim:
	$(CC) -shared -fPIC shim.c -o shim.so

pkg: all
	tar -cjf pkg.tar.bz2 intrpch shim.so launch.sh

.PHONY: clean

clean:
	rm -f intrpch shim.so pkg.tar.bz2
