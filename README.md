# df-rulasm

hacks to run Dwarf Fortress on linux with musl-libc

## compiling

```
make pkg
```

## installing

unpack the pkg.tar.bz2 in the Dwarf Fortress directory

## running

```
launch.sh
```
